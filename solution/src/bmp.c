#include "../include/bmp.h"

#define SIGNATURE 0x4D42
#define OFFSET 54
#define SIZE 40
#define PLANES 1
#define BIT_COUNT 24

static struct bmp_header create_bmp_header(uint64_t width, uint64_t height)
{
  uint64_t imgSize = width * height * sizeof(struct pixel);
  return (struct bmp_header){
      .bfType = SIGNATURE,
      .bfileSize = imgSize + OFFSET,
      .bfReserved = 0,
      .bOffBits = OFFSET,
      .biSize = SIZE,
      .biWidth = width,
      .biHeight = height,
      .biPlanes = PLANES,
      .biBitCount = BIT_COUNT,
      .biCompression = 0,
      .biSizeImage = imgSize,
      .biXPelsPerMeter = 0,
      .biYPelsPerMeter = 0,
      .biClrUsed = 0,
      .biClrImportant = 0};
}

static enum read_status validate_file(FILE *const file) { return ferror(file) != 0 ? READ_INVALID_BITS : READ_OK; }

static enum read_status validate_header(struct bmp_header *header)
{
  if (header->bfType != SIGNATURE)
  {
    return READ_INVALID_SIGNATURE;
  }
  else if (header->biBitCount != BIT_COUNT)
  {
    return READ_INVALID_HEADER;
  }
  else
  {
    return READ_OK;
  }
}

enum read_status from_bmp(FILE *const file, struct image *const img)
{
  struct bmp_header header = {0};
  if (!fread(&header, sizeof(struct bmp_header), 1, file) || validate_file(file) != READ_OK)
  {
    return READ_INVALID_BITS;
  }
  enum read_status header_validation_status = validate_header(&header);
  if (header_validation_status != READ_OK)
  {
    return header_validation_status;
  }

  uint64_t width = header.biWidth;
  uint64_t height = header.biHeight;

  *img = create_image(width, height);

  uint64_t byte_size = width * sizeof(struct pixel);

  uint8_t padding = calc_padding(byte_size);

  for (uint64_t i = 0; i < height; i++)
  {
    if (!fread(img->data + i * width, width * sizeof(struct pixel), 1, file))
    {
      return READ_INVALID_BITS;
    }
    if (fseek(file, padding, SEEK_CUR))
    {
      return READ_INVALID_BITS;
    }
  }
  return READ_OK;
}

enum write_status to_bmp(FILE *file, struct image const *img)
{
  struct bmp_header header = create_bmp_header(img->width, img->height);
  if (fwrite(&header, sizeof(struct bmp_header), 1, file) != 1)
  {
    return WRITE_ERROR;
  }
  uint64_t byte_size = img->width * sizeof(struct pixel);

  uint8_t padding = calc_padding(byte_size);

  for (uint64_t i = 0; i < img->height; i++)
  {
    if (fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, file) != img->width)
    {
      return WRITE_ERROR;
    }
    if (fseek(file, padding, SEEK_CUR))
    {
      return WRITE_ERROR;
    };
  }
  return WRITE_OK;
}
