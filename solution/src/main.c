#include "../include/bmp.h"
#include "../include/image.h"
#include "../include/rotate.h"
#include "../include/utils.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
    if (argc != 3)
    {
        printLn("Arguments wrong length");
        return -1;
    }

    FILE *input = fopen(argv[1], "rb");

    if (input == NULL)
    {
        printLn("Input file opening error");
        return -1;
    }

    FILE *output = fopen(argv[2], "wb");
    if (output == NULL)
    {
        fclose(input);
        printLn("Output file opening error");
        return -1;
    }

    struct image init_img = {0};

    if (from_bmp(input, &(init_img)) != READ_OK)
    {
        free(init_img.data);
        fclose(input);
        fclose(output);
        printLn("Error while reading input file");
        return -1;
    }
    struct image transformed_img = rotate(&init_img);

    if (to_bmp(output, &(transformed_img)) != WRITE_OK)
    {
        free(init_img.data);
        free(transformed_img.data);
        fclose(input);
        fclose(output);
        printLn("Error while writing to output file");
        return -1;
    }

    if (fclose(input) != 0)
    {
        free(init_img.data);
        free(transformed_img.data);
        printLn("Error while closing input file");
        return -1;
    }
    if (fclose(output) != 0)
    {
        free(init_img.data);
        free(transformed_img.data);
        printLn("Error while closing output file");
        return -1;
    }

    free(init_img.data);
    free(transformed_img.data);
    return 0;
}
