#include "../include/utils.h"

void printLn(char *message)
{
  printf(message, "\n");
}

uint8_t calc_padding(uint64_t byte_size)
{
  uint8_t padding = 0;

  if (byte_size % 4 != 0)
  {
    padding = ((byte_size / 4) * 4) + 4 - byte_size;
  }
  return padding;
}
